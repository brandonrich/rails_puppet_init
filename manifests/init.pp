#VARIABLES
$user = "deploy"
$group = "deploy"
$apps_home_dir = "/apps"

# GLOBAL PATH SETTING
Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

group { "deploy":
    ensure => "present",
}
->
group { "rvm":
    ensure => "present",
}
->
user_homedir { $user:
  group => $group,
  fullname => "Otto the Deployer",
  ingroups => [$group, "rvm"]
}
->
ssh::resource::known_hosts { 'add bitbucket to known_hosts':
  hosts => 'bitbucket.org',
  user => $user,
}
->
file {"apps dir":
	ensure =>	directory,
  	path => 	"${apps_home_dir}",
	mode =>		"0775",
	owner =>	$user,
        group =>	$group
  }
->
user_homedir { "vagrant":
  group => $group,
  fullname => "Vagrant Default",
  ingroups => [$group, "rvm"]
}
->
user_homedir { "ec2-user":
  group => $group,
  fullname => "EC2 User",
  ingroups => [$group, "rvm"]
}
->
class { 'nginx': }
->
exec { "chown nginx to deploy group":
    command => "chown -R root:$group /etc/nginx"
}
->
exec { "chmod sites-enabled/available":
    command => "chmod 0775 /etc/nginx/sites-*"
}
->
exec { "deploy sudo for nginx":
    command => "echo '%deploy ALL=(ALL)NOPASSWD:/usr/sbin/nginx' >> /etc/sudoers"
}
->
# I am getting the error in this issue: https://github.com/wayneeseguin/rvm/issues/2496
# A pull request was merged to fix it.  This was 2 days ago ( 12/31/2013? ).  I did not get the error BEFORE this..
# so either the pull request causes the error for me, or this puppet module is not getting the latest RVM.  So I'm trying to specify
# the version number of RVM to see what happens.
# reference to the issue, where I put in my report: https://github.com/wayneeseguin/rvm/issues/2496#issuecomment-31479728
class { 'rvm': version => '1.25.10' }

class { 'install_things_with_rvm':
    ruby_ver => 'ruby-2.0.0'
}



file {"ssl dir":
        ensure =>       directory,
        path =>         "/etc/nginx/ssl",
        mode =>         "0775",
        owner =>        $user,
        group =>        $group
  }

# duplicate definition: puppet 2.7 vs 3.x bug?
# https://www.mail-archive.com/puppet-users@googlegroups.com/msg07687.html
#user_homedir { "ec2-user":
#  group => $group,
#  fullname => "EC2 User",
#  ingroups => [$group]
#}



define user_homedir ($group, $fullname, $ingroups) {
  user { "$name":
    ensure => present,
    comment => "$fullname",
    gid => "$group",
    groups => $ingroups,
    membership => minimum,
    shell => "/bin/bash",
    home => "/home/$name",
    require => Group[$group],
  }

  exec { "$name homedir":
    command => "/bin/cp -R /etc/skel /home/$name; /bin/chown -R $name:$group /home/$name",
    creates => "/home/$name",
    require => User[$name],
  }
}



# remove host firewall
resources { "firewall":
  purge => true
}


package{ 'nodejs':
	ensure => 'installed',
	provider => 'yum'
}



#->
#  # SET UP SUDOERS
#  class { 'sudo': }
#  ->
#  sudo::conf { 'deploy group':
#    priority => 10,
#    content  => '%deploy ALL=(ALL) NOPASSWD:/etc/nginx/sbin/nginx',
#  }
#  ->
#  sudo::conf { 'deploy_tty':
#    priority => 10,
#    content  => 'Defaults:deploy !requiretty'
#  }
#  ->
#  sudo::conf { 'admins':
#    priority => 10,
#    content  => 'User_Alias ADMINS = brich vagrant ec2-user',
#  }
#  ->
#  sudo::conf { 'admin_privs':
#    priority => 10,
#    content  => 'ADMINS  ALL=(ALL)       ALL'
#  }

# RVM module must exist and have had the libcurl dependency file modified to work
# on Amazon Linux (by changing the default to libcurl-devel in /manifests/dependencies/centos.pp)
class install_things_with_rvm (
    $ruby_ver = 'ruby-1.9.3',
    $rails_ver = '4.0.2'
){
    require rvm

    rvm_system_ruby {
      $ruby_ver:
        ensure => 'present',
        default_use => true;
    }
    rvm_gem {
      'bundler':
        name => 'bundler',
        ruby_version => $ruby_ver,
        ensure => latest,
        require => Rvm_system_ruby[$ruby_ver];
    }
    rvm_gem {
      'rails':
        name => 'rails',
        ruby_version => $ruby_ver,
        ensure => $rails_ver,
        require => Rvm_system_ruby[$ruby_ver];
    }
    package{
      'sqlite-devel':
        ensure => 'installed',
        provider => 'yum'
    }
}

package { 'redis':
	ensure => installed,
	provider => 'yum'
}

package { 'npm':
	ensure => installed,
	provider => 'yum'
}
package { 'ImageMagick':
	ensure => installed,
	provider => 'yum'
}
